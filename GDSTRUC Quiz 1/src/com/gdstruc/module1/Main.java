package com.gdstruc.module1;

public class Main {

    public static void main(String[] args) {

        int[] numbers = new int[15];
        numbers[0] = 10;
        numbers[1] = 50;
        numbers[2] = 70;
        numbers[3] = 30;
        numbers[4] = 40;
        numbers[5] = 60;
        numbers[6] = 100;
        numbers[7] = 90;
        numbers[8] = 80;
        numbers[9] = 150;
        numbers[10] = 20;
        numbers[11] = 130;
        numbers[12] = 140;
        numbers[13] = 120;
        numbers[14] = 110;

        // Print the contents of the array before sorting it
        System.out.println("Unsorted Array:");
        printArrayElements(numbers);

        bubbleSort(numbers); // Then bubble sort the elements in descending order
        System.out.println("\n\nBubble sort (Descending):");
        printArrayElements(numbers);

        selectionSort(numbers); // Lastly do selection sort in descending order
        System.out.println("\n\nSelection sort (Descending):");
        printArrayElements(numbers);
    }

    private static void bubbleSort(int[] arr)
    {
        for (int lastSortedIndex = arr.length - 1; lastSortedIndex > 0; lastSortedIndex--)
        {
            for (int i = 0; i < lastSortedIndex; i++)
            {
                if (arr[i] < arr[i + 1])
                {
                    int temp = arr[i];
                    arr[i] = arr[i + 1];
                    arr[i + 1] = temp;
                }
            }
        }
    }

    private static void selectionSort(int[] arr)
    {
        for (int lastSortedIndex = arr.length - 1; lastSortedIndex > 0; lastSortedIndex--)
        {
            int smallestIndex = 0;
            for (int i = 1; i <= lastSortedIndex; i++)
            {
                if (arr[i] < arr[smallestIndex])
                {
                    smallestIndex = i;
                }
            }
            int temp = arr[lastSortedIndex];
            arr[lastSortedIndex] = arr[smallestIndex];
            arr[smallestIndex] = temp;
        }
    }

    private static void printArrayElements(int[] arr)
    {
        for(int j : arr)
        {
            System.out.print(j + " ");
        }
    }

}












