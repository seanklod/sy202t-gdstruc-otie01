package com.gdstruc.midterms;

public class Card {
    private int number;
    private String name;

    // constructor
    public Card(int number, String name) {
        this.number = number;
        this.name = name;
    }

    @Override
    public String toString() {
        return "Card {" +
                " " + number +
                ") " + name +
                " }";
    }

    // getters and setters
    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
