package com.gdstruc.midterms;

import java.util.LinkedList;
import java.util.ListIterator;

public class CardStack {
    private LinkedList<Card> stack;
    private String stackName;

    public CardStack(String name)
    {
        stack = new LinkedList<Card>();
        stackName = name;
    }

    public String getStackName() {
        return stackName;
    }

    public void setStackName(String stackName) {
        this.stackName = stackName;
    }

    public void push(Card card)
    {
        stack.push(card);
    }

    public boolean isEmpty()
    {
        return  stack.isEmpty();
    }

    public Card pop()
    {
        return stack.pop();
    }

    public Card peek()
    {
        return stack.peek();
    }

    public void printStack()
    {
        ListIterator<Card> iterator = stack.listIterator();

        while (iterator.hasNext())
        {
            System.out.println(iterator.next());
        }
    }

    public int getStackSize()
    {
        return stack.size();
    }
}
