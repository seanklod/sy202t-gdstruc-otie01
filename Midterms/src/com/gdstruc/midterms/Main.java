package com.gdstruc.midterms;

import java.util.Random;
import java.util.Scanner;

public class Main {

    // function for drawing cards
    private static void drawCards(CardStack stackToPop, CardStack stackToPush)
    {
        // randomizes numbers of card to draw
        Random card = new Random();
        int cardsToDraw;
        cardsToDraw = 1 + card.nextInt(8);

        // if u rolled a number of cards greater than the current size, set cardsToDraw to remaining size of stack
        if (stackToPop.getStackSize() < cardsToDraw)
        {
            cardsToDraw = cardsToDraw = stackToPop.getStackSize();
        }

        System.out.println("You draw " + cardsToDraw + " cards from the " + stackToPop.getStackName() + "!");

        for (int i = cardsToDraw - 1; i >= 0; i--)
        {
            stackToPush.push(stackToPop.pop());
        }
    }

    // function for discarding cards
    private static void discard(CardStack stackToPop, CardStack stackToPush)
    {
        // randomizes numbers of card to discard
        Random card = new Random();
        int cardsToDiscard;
        cardsToDiscard = 1 + card.nextInt(7);

        // if u rolled a number greater than the current size, set cardsToDiscard to remaining size of stack
        if (stackToPop.getStackSize() < cardsToDiscard)
        {
            cardsToDiscard = cardsToDiscard = stackToPop.getStackSize();
        }

        System.out.println("You discard " + cardsToDiscard + " cards from your hand!");

        for (int i = cardsToDiscard - 1; i >= 0; i--)
        {
            stackToPush.push(stackToPop.pop());
        }
    }

    public static void main(String[] args) {

        // initiate all stacks
        CardStack playerDeck = new CardStack("Player Deck");
        CardStack discardPile = new CardStack("Discarded Pile");
        CardStack hand = new CardStack("Hand Stack");

        // add 30 cards to player deck
        playerDeck.push(new Card(1, "Spade"));
        playerDeck.push(new Card(2, "Spade"));
        playerDeck.push(new Card(3, "Spade"));
        playerDeck.push(new Card(4, "Spade"));
        playerDeck.push(new Card(5, "Spade"));
        playerDeck.push(new Card(6, "Hearts"));
        playerDeck.push(new Card(7, "Hearts"));
        playerDeck.push(new Card(8, "Hearts"));
        playerDeck.push(new Card(9, "Hearts"));
        playerDeck.push(new Card(10, "Hearts"));
        playerDeck.push(new Card(11, "Diamond"));
        playerDeck.push(new Card(12, "Diamond"));
        playerDeck.push(new Card(13, "Diamond"));
        playerDeck.push(new Card(14, "Diamond"));
        playerDeck.push(new Card(15, "Diamond"));
        playerDeck.push(new Card(16, "Club"));
        playerDeck.push(new Card(17, "Club"));
        playerDeck.push(new Card(18, "Club"));
        playerDeck.push(new Card(19, "Club"));
        playerDeck.push(new Card(20, "Club"));
        playerDeck.push(new Card(21, "Reverse Card"));
        playerDeck.push(new Card(22, "Reverse Card"));
        playerDeck.push(new Card(23, "Reverse Card"));
        playerDeck.push(new Card(24, "Reverse Card"));
        playerDeck.push(new Card(25, "Reverse Card"));
        playerDeck.push(new Card(26, "Skip"));
        playerDeck.push(new Card(27, "Skip"));
        playerDeck.push(new Card(28, "Skip"));
        playerDeck.push(new Card(29, "Skip"));
        playerDeck.push(new Card(30, "Skip"));

        System.out.println("Enter any key to start: ");
        Scanner scanner = new Scanner(System.in);
        String input = scanner.nextLine();

        // continue looping as long as player deck contains cards
        while (playerDeck.getStackSize() != 0)
        {
            // randomize command
            Random command = new Random();
            int number = 1 + command.nextInt(3);;

            // draw from player deck
            if (number == 1)
            {
                System.out.println("\nCommand: Draw cards from the Player Deck");
                drawCards(playerDeck, hand);
            }

            // draw from discarded pile
            if (number == 2)
            {
                System.out.println("\nCommand: Draw cards from the Discarded Pile");

                // if no cards in discard pile, prompt to roll again
                if (discardPile.isEmpty())
                {
                    System.out.println("There are no cards in the discarded pile! Please roll again.");
                }

                else
                {
                    drawCards(discardPile, hand);
                }
            }

            // throw cards from your hand
            if (number == 3)
            {
                System.out.println("\nCommand: Throw cards from your hand");

                // if no cards in player hands, roll again
                if (hand.isEmpty())
                {
                    System.out.println("There are no cards in your hand! Please roll again.");
                }

                else
                {
                    discard(hand, discardPile);
                }
            }

            System.out.println("Number of cards in your hand: " + hand.getStackSize() + "\n" + "Player's hand: ");
            hand.printStack();

            //Print the size of the deck and the discarded cards
            System.out.println("\nNumber of cards on the player deck: " + playerDeck.getStackSize());
            System.out.println("Number of cards on the discarded pile: " + discardPile.getStackSize());

            // Get input from user before continuing (acts like system pause)
            System.out.println("\nEnter any key to continue: ");
            Scanner scanner2 = new Scanner(System.in);
            String input2 = scanner2.nextLine();

        }

        // game ends when no cards remain on player deck
        System.out.println("======= NO CARDS LEFT IN THE PLAYER DECK. GAME OVER! =======");
    }
}