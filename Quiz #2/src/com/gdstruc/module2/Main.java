package com.gdstruc.module2;

public class Main {

    public static void main(String[] args) {

        Player eren = new Player (10, "Eren", 20);
        Player armin = new Player (15, "Armin", 30);
        Player annie = new Player (30, "Annie", 40);
        Player reiner = new Player(50, "Reiner", 30);
        Player levi = new Player (1, "Levi", 900);

        PlayerLinkedList playerLinkedList = new PlayerLinkedList();

        // add elements to list
        playerLinkedList.addToFront(eren);
        playerLinkedList.addToFront(armin);
        playerLinkedList.addToFront(annie);

        playerLinkedList.printList();
        playerLinkedList.getSize();

        playerLinkedList.contains("Eren");
        playerLinkedList.indexOf("Eren");

        playerLinkedList.deleteFromFirst();
        playerLinkedList.printList();
        playerLinkedList.getSize(); // get the size after first element is deleted

        playerLinkedList.indexOf("Annie"); // search for element that is not in the list
        playerLinkedList.contains("Annie");

        // add new elements to list then print and get size
        playerLinkedList.addToFront(reiner);
        playerLinkedList.addToFront(levi);
        playerLinkedList.printList();
        playerLinkedList.getSize();

        playerLinkedList.contains("Eren");
        playerLinkedList.indexOf("Eren");
    }
}
