package com.gdstruc.module2;

public class PlayerLinkedList {
    private PlayerNode head;
    private PlayerNode tail;

    public void printList() {
        PlayerNode current = head;

        System.out.print("HEAD -> ");
        while (current != null) {
            System.out.print(current.getPlayer().getName());
            System.out.print(" -> ");
            tail = current; // set current as previous
            current = current.getNextPlayer();
        }
        System.out.println("null");
    }

    // add element
    public void addToFront(Player player) {
        PlayerNode playerNode = new PlayerNode(player);
        playerNode.setNextPlayer(head);
        playerNode.setPreviousPlayer(tail);
        head = playerNode;
        tail = playerNode;
    }

    public void deleteFromFirst() {
        head = head.getNextPlayer();
    }

    public void getSize() {
        int size = 0;
        PlayerNode current = head;

        while (current != null) {
            size++;
            current = current.getNextPlayer();
        }

        System.out.println("The size of the array is: " + size);
    }

    public void contains(String name)
    {
        boolean isContained = false;
        PlayerNode current = head;
        while (current != null && !isContained)
        {
            String playerName = current.getPlayer().getName();

            if (playerName.equals(name))
            {
                System.out.println("true");
                isContained = true;
            }

            if (!playerName.equals(name))
            {
                current = current.getNextPlayer();
            }
        }

        if (current == null)
        {
            System.out.println("false");
        }
    }

    public void indexOf(String name)
    {
        int index = 0;
        boolean isFound = false;
        PlayerNode current = head;
        while (current != null && !isFound)
        {
            if (current.getPlayer().getName().equals(name))
            {
                System.out.println("Object is at index: " + index);
                isFound = true;
            }

            if (!current.getPlayer().getName().equals(name))
            {
                current = current.getNextPlayer();
                index++;
            }
        }

        if (current == null)
        {
            System.out.println("Object does not exist.");
        }
    }
}
