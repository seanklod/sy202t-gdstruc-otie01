package com.gdstruc.module4;

import java.util.Random;
import java.util.Scanner;

public class Main {

    public static void main(String[] args)
    {
        ArrayQueue queue = new ArrayQueue(5);
        ArrayQueue playerList = new ArrayQueue(50);

        playerList.add(new Player(1, "Diluc"));
        playerList.add(new Player(2, "Childe"));
        playerList.add(new Player(3, "Zhongli"));
        playerList.add(new Player(4, "Venti"));
        playerList.add(new Player(5, "Klee"));
        playerList.add(new Player(6, "Albedo"));
        playerList.add(new Player(7, "Ganyu"));
        playerList.add(new Player(8, "Xiao"));
        playerList.add(new Player(9, "Hutao"));
        playerList.add(new Player(10, "Keqing"));
        playerList.add(new Player(11, "Jean"));
        playerList.add(new Player(12, "Qiqi"));
        playerList.add(new Player(13, "Mona"));
        playerList.add(new Player(14, "Eula"));
        playerList.add(new Player(15, "Aether"));
        playerList.add(new Player(16, "Lisa"));
        playerList.add(new Player(17, "Kaeya"));
        playerList.add(new Player(18, "Amber"));
        playerList.add(new Player(19, "Lumine"));
        playerList.add(new Player(20, "Rosaria"));
        playerList.add(new Player(21, "Barbara"));
        playerList.add(new Player(22, "Fischl"));
        playerList.add(new Player(23, "Bennett"));
        playerList.add(new Player(24, "Diona"));
        playerList.add(new Player(25, "Ningguang"));
        playerList.add(new Player(26, "Beidou"));
        playerList.add(new Player(27, "Xingqiu"));
        playerList.add(new Player(28, "Chongyun"));
        playerList.add(new Player(29, "Noelle"));
        playerList.add(new Player(30, "Sucrose"));
        playerList.add(new Player(31, "Xinyan"));
        playerList.add(new Player(32, "Xiangling"));
        playerList.add(new Player(33, "Yanfei"));
        playerList.add(new Player(34, "Mimi"));
        playerList.add(new Player(35, "Esmeralda"));
        playerList.add(new Player(36, "Khufra"));
        playerList.add(new Player(37, "Scaramouche"));
        playerList.add(new Player(38, "Ayaka"));
        playerList.add(new Player(39, "Masha"));
        playerList.add(new Player(40, "Freya"));
        playerList.add(new Player(41, "Layla"));
        playerList.add(new Player(42, "Carmilla"));
        playerList.add(new Player(43, "Cecilion"));
        playerList.add(new Player(44, "Zhask"));
        playerList.add(new Player(45, "Karrie"));
        playerList.add(new Player(46, "Karina"));
        playerList.add(new Player(47, "Selena"));
        playerList.add(new Player(48, "Alice"));
        playerList.add(new Player(49, "Odette"));
        playerList.add(new Player(50, "Lancelot"));

        int games = 0;

        System.out.println("---MATCHMAKING IN PROGRESS---");

        while (games < 10)
        {
            Random number = new Random();
            int playerToQueue = 1 + number.nextInt(7);

            System.out.println("Ongoing games: " + games + "\n");

            System.out.println("Number of players about to enter matchmaking: " + playerToQueue + "\n");

            for (int i = 0; i < playerToQueue; i++)
            {
                if (playerList.size() > 0)
                {
                    queue.add(playerList.remove());
                }
            }

            System.out.println("Current number of players in the queue: " + queue.size());

            if (queue.size() < 5)
            {
                System.out.println("Not enough players in queue. Please wait..");
            }

            if (queue.size() >= 5)
            {
                System.out.println("Match found! \n\nPlayers in match: ");
                for (int i = 0; i < 5; i++)
                {
                    System.out.println(queue.peek());
                    queue.remove();
                }

                games++;
            }

            System.out.println("Enter any key to continue: ");
            Scanner scanner = new Scanner(System.in);
            String input = scanner.nextLine();
        }

        if (games >= 10)
        {
            System.out.println("MATCHING COMPLETE! SERVER IS NOW FULL");
        }
    }
}
