package com.gdstruc.module4;

import java.util.Objects;

public class Player {
    private int id;
    private String name;

    // constructor
    public Player(int id, String name) {
        this.id = id;
        this.name = name;
    }

    @Override
    public String toString() {
        return "Player {" +
                " " + id +
                ") " + name +
                " }";
    }

    // getters and setters
    public int getLevel() {
        return id;
    }

    public void setLevel(int level) {
        this.id = level;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}

