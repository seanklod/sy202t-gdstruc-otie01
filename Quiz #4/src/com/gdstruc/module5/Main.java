package com.gdstruc.module5;

public class Main {

    public static void main(String[] args) {

        Player diluc = new Player(1, "Diluc");
        Player venti = new Player(2, "Venti");
        Player zhongli = new Player(3, "Zhongli");
        Player childe = new Player(4, "Childe");

        SimpleHashtable hashtable = new SimpleHashtable();
        hashtable.put(diluc.getName(), diluc);
        hashtable.put(venti.getName(), venti);
        hashtable.put(zhongli.getName(), zhongli);

        hashtable.printHashtable();
        System.out.println("Locating element: Venti");
        System.out.println("Element located: " + hashtable.get("Venti"));
        System.out.println("Deleting element: Venti\n");
        hashtable.remove("Venti");
        hashtable.printHashtable();

        // check if the value inside the index has really been deleted by inserting another element into it
        System.out.println("Replacing with: Childe\n");
        hashtable.put(childe.getName(), childe);
        hashtable.printHashtable();

        // check if element does not exist
        System.out.println("Locating element: Venti");
        hashtable.get("Venti");
        System.out.println("Deleting element: Venti");
        hashtable.remove("Venti");
    }
}
