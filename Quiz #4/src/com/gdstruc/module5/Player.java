package com.gdstruc.module5;

import java.util.Objects;

public class Player {
    private int id;
    private String name;

    // constructor
    public Player(int id, String name) {
        this.id = id;
        this.name = name;
    }

    @Override
    public String toString() {
        return "Player {" +
                " " + id +
                ") " + name +
                " }";
    }

    // getters and setters
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}

