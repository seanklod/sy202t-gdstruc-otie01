package com.gdstruc.module5;

public class StoredPlayer {
    public String key;
    public Player value;

    public StoredPlayer(String key, Player value) {
        this.key = key;
        this.value = value;
    }

    public void delete()
    {
        value = null;
        key = null;
    }
}
