package com.gdstruc.module6;

public class Main {
    public static void main(String[] args) {

        // initiate int array
        int array[] = { 100, 156, 30, -19, 25, 47, 77 };

        // search for value that exist on the last index
        System.out.println("Search for element: 77");
        pendulumSearch(array, 77);
        System.out.println("\n");

        // this will also print the process of checking the elements in each index and whether
        // it follows the pendulum swing movement (index 0, then last index, then index 1, then second to the last index)

        // Search for value that exists on index 0
        System.out.println("Search for element: 100");
        pendulumSearch(array, 100);
        System.out.println("\n");

        // Search for value that exists on the middle index
        System.out.println("Search for element: -19");
        pendulumSearch(array, -19);
        System.out.println("\n");

        // Search for value that exists
        System.out.println("Search for element: 156");
        pendulumSearch(array, 156);
        System.out.println("\n");

        // Search for value that does not exist
        System.out.println("Search for element: 999");
        pendulumSearch(array, 999);
        System.out.println("\n");
    }

    private static void pendulumSearch(int[] array, int searchValue)
    {
        boolean isValueFound = false;

        int middle = array.length/2; // get the middle of the array length
        int index = 0;
        int start = 0;
        int end = array.length - 1;


        while (isValueFound == false && index != middle)
        {

            if (array[start] == searchValue)
            {
                isValueFound = true;
                System.out.println("Element is located at index: " + index);
            }

            if (array[start] != searchValue)
            {
                System.out.println("Index  " + start + ": " + array[start] + " is not equal to " + searchValue);
                if (start < middle) { start++;} // only increment start or left index if it's lesser than middle index
                index = end;
            }

            if (array[end] == searchValue)
            {
                isValueFound = true;
                System.out.println("Element is located at: " + index);
            }

            if (array[end] != searchValue)
            {
                System.out.println("Index  " + end + ": " + array[end] + " is not equal to " + searchValue);
                if (end > middle) {end--;} // only decrement end or right index if it's greater than middle index
                index = start;
            }

            // run if all indices left and right of the middle has already been checked
            if (index == middle)
            {
                if (array[index] != searchValue)
                {
                    System.out.println("Index  " + middle + ": " + array[middle] + " is not equal to " + searchValue);
                }
                else
                {
                    isValueFound = true;
                    System.out.println("Element is located at: " + index);
                }
            }

        }

        // if entire array is checked and value is not found
        if (isValueFound == false)
        {
            System.out.println("Searching done. Element does not exist!");
        }
    }
}

