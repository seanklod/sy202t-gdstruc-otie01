package com.gdstruc.module7;

public class Main {

    public static void main(String[] args) {
	    Tree tree = new Tree();

        tree.insert(10);
        tree.insert(9);
        tree.insert(15);
        tree.insert(20);
        tree.insert(6);
        tree.insert(1);
        tree.insert(-10);
        tree.insert(99);
        tree.insert(70);
        tree.insert(132);

        System.out.println("The contents of the tree in descending order: ");
        tree.traverseInOrderDescend();

        System.out.println("\nLocating minimum value in the tree...\n Found: " + tree.getMin());
        System.out.println("Locating maximum value in the tree...\n Found: " + tree.getMax());

        System.out.println("\nLocate element: 21");
        System.out.println("Element is " + tree.get(21));

        System.out.println("\nLocate element: 1");
        System.out.println("Element is at " + tree.get(1));
    }
}
